#pragma once

#include "Types.h"

namespace VectorFit {

struct PreviousTransportHelper {
  template<class F, size_t... Is>
  static inline constexpr std::array<PRECISION, 5*sizeof...(Is)> generate (
    const F& get_element,
    std::index_sequence<Is...>
  ) {
    return {{
      get_element(Is)[0]...,
      get_element(Is)[1]...,
      get_element(Is)[2]...,
      get_element(Is)[3]...,
      get_element(Is)[4]...
    }};
  }
};

struct PreviousNoiseMatrixHelper {
  template<class F, size_t... Is>
  static inline constexpr std::array<PRECISION, 15*sizeof...(Is)> generate (
    const F& get_element,
    std::index_sequence<Is...>
  ) {
    return {{
      get_element(Is)[0]...,
      get_element(Is)[1]...,
      get_element(Is)[2]...,
      get_element(Is)[3]...,
      get_element(Is)[4]...,
      get_element(Is)[5]...,
      get_element(Is)[6]...,
      get_element(Is)[7]...,
      get_element(Is)[8]...,
      get_element(Is)[9]...,
      get_element(Is)[10]...,
      get_element(Is)[11]...,
      get_element(Is)[12]...,
      get_element(Is)[13]...,
      get_element(Is)[14]...
    }};
  }
};

struct InitialCovarianceGenerator {
  template<size_t... Is>
  static inline constexpr std::array<PRECISION, 15*sizeof...(Is)> generate (
    std::index_sequence<Is...>
  ) {
    return {{
      m_initialCovarianceValues[0 + Is*0]...,
      m_initialCovarianceValues[1 + Is*0]...,
      m_initialCovarianceValues[2 + Is*0]...,
      m_initialCovarianceValues[3 + Is*0]...,
      m_initialCovarianceValues[4 + Is*0]...,
      m_initialCovarianceValues[5 + Is*0]...,
      m_initialCovarianceValues[6 + Is*0]...,
      m_initialCovarianceValues[7 + Is*0]...,
      m_initialCovarianceValues[8 + Is*0]...,
      m_initialCovarianceValues[9 + Is*0]...,
      m_initialCovarianceValues[10 + Is*0]...,
      m_initialCovarianceValues[11 + Is*0]...,
      m_initialCovarianceValues[12 + Is*0]...,
      m_initialCovarianceValues[13 + Is*0]...,
      m_initialCovarianceValues[14 + Is*0]...
    }};
  }
};

struct InitialStateGenerator {
  template<class F, size_t... Is>
  static inline constexpr std::array<PRECISION, 5*sizeof...(Is)> generate (
    const F& get_element,
    std::index_sequence<Is...>
  ) {
    return {{
      get_element(Is)[0]...,
      get_element(Is)[1]...,
      get_element(Is)[2]...,
      get_element(Is)[3]...,
      get_element(Is)[4]...
    }};
  }
};

struct ArrayGenCommon {
  template<size_t W>
  static inline constexpr uint16_t mask () {
    return (W==16 ? 0xFFFF : (W==8 ? 0xFF : (W==4 ? 0x0F : 0x03)));
  }

  template<size_t W>
  static inline constexpr std::array<PRECISION, 5*W> getPreviousTransportVector (
    const std::array<Sch::Item, W>& n,
    const std::vector<VectorFit::Track>& t
  ) {
    return PreviousTransportHelper::generate(
      [&n, &t] (int j) -> Mem::View::TrackVector { return t[n[j].trackIndex].m_nodes[n[j].nodeIndex+1].m_nodeParameters.m_transportVector; },
      std::make_index_sequence<W>{}
    );
  }

  template<size_t W>
  static inline constexpr std::array<PRECISION, 15*W> getPreviousNoiseMatrix (
    const std::array<Sch::Item, W>& n,
    const std::vector<VectorFit::Track>& t
  ) {
    return PreviousNoiseMatrixHelper::generate(
        [&n, &t] (int j) -> Mem::View::TrackSymMatrix { return t[n[j].trackIndex].m_nodes[n[j].nodeIndex+1].m_nodeParameters.m_noiseMatrix; },
      std::make_index_sequence<W>{}
    );
  }

  template<size_t W>
  static inline constexpr std::array<PRECISION, 15*W> generateInitialCovariance () {
    return InitialCovarianceGenerator::generate(std::make_index_sequence<W>{});
  }
};

template<class D>
struct ArrayGen {
  template<size_t W>
  static inline constexpr std::array<PRECISION, 5*W> generateInitialState (
    const std::array<Sch::Item, W>& n,
    const std::vector<VectorFit::Track>& t
  );
};

template<>
struct ArrayGen<Op::Forward> {
  template<size_t W>
  static inline constexpr std::array<PRECISION, 5*W> generateInitialState (
    const std::array<Sch::Item, W>& n,
    const std::vector<VectorFit::Track>& t
  ) {
    return InitialStateGenerator::generate(
      [&n, &t] (int j) -> Mem::View::TrackVector { return t[n[j].trackIndex].m_nodes[n[j].nodeIndex].m_nodeParameters.m_referenceVector; },
      std::make_index_sequence<W>{}
    );
  }
};

template<>
struct ArrayGen<Op::Backward> {
  template<size_t W>
  static inline constexpr std::array<PRECISION, 5*W> generateInitialState (
    const std::array<Sch::Item, W>& n,
    const std::vector<VectorFit::Track>& t
  ) {
    return InitialStateGenerator::generate(
      [&n, &t] (int j) -> Mem::View::TrackVector { return t[n[j].trackIndex].m_nodes[n[j].nodeIndex].m_forwardState.m_updatedState; },
      std::make_index_sequence<W>{}
    );
  }
};

static _type_aligned std::array<PRECISION, 15*VECTOR_WIDTH> m_initialVectorCovariance = 
  ArrayGenCommon::generateInitialCovariance<VECTOR_WIDTH>();

}
