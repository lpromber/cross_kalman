#pragma once

#include "../Types.h"
#include "Math.h"

namespace VectorFit {

namespace Scalar {

template<class D>
inline void initialUpdate (
  FitNode& node
) {
  Math::update (
    node.get<Op::NodeParameters, Op::ReferenceVector>(),
    node.get<Op::NodeParameters, Op::ProjectionMatrix>(),
    node.get<Op::NodeParameters, Op::ReferenceResidual>(),
    node.get<Op::NodeParameters, Op::ErrMeasure>(),
    node.get<Op::NodeParameters, Op::ReferenceResidual>(),
    m_initialScalarCovariance,
    node.get<D, Op::StateVector>(),
    node.get<D, Op::Covariance>(),
    node.get<D, Op::Chi2>()
  );
}

template<class D>
inline void update (
  FitNode& node
) {
  if (node.m_type == HitOnTrack) {
    Math::update (
      node.get<Op::NodeParameters, Op::ReferenceVector>(),
      node.get<Op::NodeParameters, Op::ProjectionMatrix>(),
      node.get<Op::NodeParameters, Op::ReferenceResidual>(),
      node.get<Op::NodeParameters, Op::ErrMeasure>(),
      node.get<D, Op::StateVector>(),
      node.get<D, Op::Covariance>(),
      node.get<D, Op::StateVector>(),
      node.get<D, Op::Covariance>(),
      node.get<D, Op::Chi2>()
    );
  }
}

}

}
