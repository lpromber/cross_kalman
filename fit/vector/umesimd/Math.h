#pragma once

#include <array>
#include "../../MemStore.h"
#include "../../Types.h"

namespace VectorFit {

namespace Vector {

template<unsigned W>
struct MathCommon {
  static constexpr inline unsigned symid (const int row, const int col) {
    return ((row * (row + 1)) / 2) + col;
  }

  template <size_t... Is>
  static constexpr inline typename Vectype<W>::booltype makeBoolHitOnTrack (
    const std::array<Sch::Item, W>& nodes,
    const std::vector<VectorFit::Track>& tracks,
    std::index_sequence<Is...>
  ) {
    return typename Vectype<W>::booltype(tracks[nodes[Is].trackIndex].m_nodes[nodes[Is].nodeIndex].m_type == HitOnTrack...);
  }

  /**
   * @brief      Similarity transform, manually horizontally vectorised
   *
   * @param[in]  tm_p  Transport matrix
   * @param[in]  uc_p  Previous update covariance
   * @param      pc_p  Predicted covariance
   */
  static inline void similarity_5_5 (
    fp_ptr_64_const tm_p,
    fp_ptr_64_const uc_p,
    fp_ptr_64 pc_p
  ) {
    using vectype = typename Vectype<W>::type;
    std::array<vectype, 25> tm;
    std::array<vectype, 15> uc;
    std::array<vectype, 15> pc;
    std::array<vectype, 5> v;

    // Load tm
    tm[0].loada(tm_p + 0*W);
    tm[1].loada(tm_p + 1*W);
    tm[2].loada(tm_p + 2*W);
    tm[3].loada(tm_p + 3*W);
    tm[4].loada(tm_p + 4*W);
    tm[5].loada(tm_p + 5*W);
    tm[6].loada(tm_p + 6*W);
    tm[7].loada(tm_p + 7*W);
    tm[8].loada(tm_p + 8*W);
    tm[9].loada(tm_p + 9*W);
    tm[10].loada(tm_p + 10*W);
    tm[11].loada(tm_p + 11*W);
    tm[12].loada(tm_p + 12*W);
    tm[13].loada(tm_p + 13*W);
    tm[14].loada(tm_p + 14*W);
    tm[15].loada(tm_p + 15*W);
    tm[16].loada(tm_p + 16*W);
    tm[17].loada(tm_p + 17*W);
    tm[18].loada(tm_p + 18*W);
    tm[19].loada(tm_p + 19*W);
    tm[20].loada(tm_p + 20*W);
    tm[21].loada(tm_p + 21*W);
    tm[22].loada(tm_p + 22*W);
    tm[23].loada(tm_p + 23*W);
    tm[24].loada(tm_p + 24*W);

    // Load uc
    uc[0].loada(uc_p + 0*W);
    uc[1].loada(uc_p + 1*W);
    uc[2].loada(uc_p + 2*W);
    uc[3].loada(uc_p + 3*W);
    uc[4].loada(uc_p + 4*W);
    uc[5].loada(uc_p + 5*W);
    uc[6].loada(uc_p + 6*W);
    uc[7].loada(uc_p + 7*W);
    uc[8].loada(uc_p + 8*W);
    uc[9].loada(uc_p + 9*W);
    uc[10].loada(uc_p + 10*W);
    uc[11].loada(uc_p + 11*W);
    uc[12].loada(uc_p + 12*W);
    uc[13].loada(uc_p + 13*W);
    uc[14].loada(uc_p + 14*W);

    v[0] = uc[0] *tm[0]+uc[1] *tm[1]+uc[3] *tm[2]+uc[6] *tm[3]+uc[10]*tm[4];
    v[1] = uc[1] *tm[0]+uc[2] *tm[1]+uc[4] *tm[2]+uc[7] *tm[3]+uc[11]*tm[4];
    v[2] = uc[3] *tm[0]+uc[4] *tm[1]+uc[5] *tm[2]+uc[8] *tm[3]+uc[12]*tm[4];
    v[3] = uc[6] *tm[0]+uc[7] *tm[1]+uc[8] *tm[2]+uc[9] *tm[3]+uc[13]*tm[4];
    v[4] = uc[10]*tm[0]+uc[11]*tm[1]+uc[12]*tm[2]+uc[13]*tm[3]+uc[14]*tm[4];

    pc[0]  = tm[0] *v[0] + tm[1] *v[1] + tm[2] *v[2] + tm[3] *v[3] + tm[4] *v[4];
    pc[1]  = tm[5] *v[0] + tm[6] *v[1] + tm[7] *v[2] + tm[8] *v[3] + tm[9] *v[4];
    pc[3]  = tm[10]*v[0] + tm[11]*v[1] + tm[12]*v[2] + tm[13]*v[3] + tm[14]*v[4];
    pc[6]  = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    pc[10] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];

    v[0] =  uc[0] *tm[5]+uc[1] *tm[6]+uc[3] *tm[7]+uc[6] *tm[8]+uc[10]*tm[9];
    v[1] =  uc[1] *tm[5]+uc[2] *tm[6]+uc[4] *tm[7]+uc[7] *tm[8]+uc[11]*tm[9];
    v[2] =  uc[3] *tm[5]+uc[4] *tm[6]+uc[5] *tm[7]+uc[8] *tm[8]+uc[12]*tm[9];
    v[3] =  uc[6] *tm[5]+uc[7] *tm[6]+uc[8] *tm[7]+uc[9] *tm[8]+uc[13]*tm[9];
    v[4] =  uc[10]*tm[5]+uc[11]*tm[6]+uc[12]*tm[7]+uc[13]*tm[8]+uc[14]*tm[9];

    pc[2]  = tm[5] *v[0] + tm[6] *v[1] + tm[7] *v[2] + tm[8] *v[3] + tm[9] *v[4];
    pc[4]  = tm[10]*v[0] + tm[11]*v[1] + tm[12]*v[2] + tm[13]*v[3] + tm[14]*v[4];
    pc[7]  = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    pc[11] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];

    v[0] = uc[0] *tm[10]+uc[1] *tm[11]+uc[3] *tm[12]+uc[6] *tm[13]+uc[10]*tm[14];
    v[1] = uc[1] *tm[10]+uc[2] *tm[11]+uc[4] *tm[12]+uc[7] *tm[13]+uc[11]*tm[14];
    v[2] = uc[3] *tm[10]+uc[4] *tm[11]+uc[5] *tm[12]+uc[8] *tm[13]+uc[12]*tm[14];
    v[3] = uc[6] *tm[10]+uc[7] *tm[11]+uc[8] *tm[12]+uc[9] *tm[13]+uc[13]*tm[14];
    v[4] = uc[10]*tm[10]+uc[11]*tm[11]+uc[12]*tm[12]+uc[13]*tm[13]+uc[14]*tm[14];

    pc[5]  = tm[10]*v[0] + tm[11]*v[1] + tm[12]*v[2] + tm[13]*v[3] + tm[14]*v[4];
    pc[8]  = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    pc[12] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];

    v[0] = uc[0] *tm[15]+uc[1] *tm[16]+uc[3] *tm[17]+uc[6] *tm[18]+uc[10]*tm[19];
    v[1] = uc[1] *tm[15]+uc[2] *tm[16]+uc[4] *tm[17]+uc[7] *tm[18]+uc[11]*tm[19];
    v[2] = uc[3] *tm[15]+uc[4] *tm[16]+uc[5] *tm[17]+uc[8] *tm[18]+uc[12]*tm[19];
    v[3] = uc[6] *tm[15]+uc[7] *tm[16]+uc[8] *tm[17]+uc[9] *tm[18]+uc[13]*tm[19];
    v[4] = uc[10]*tm[15]+uc[11]*tm[16]+uc[12]*tm[17]+uc[13]*tm[18]+uc[14]*tm[19];

    pc[9]  = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    pc[13] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];

    v[0] = uc[0] *tm[20]+uc[1]*tm[21]+uc[3]*tm[22]+uc[6]*tm[23]+uc[10]*tm[24];
    v[1] = uc[1] *tm[20]+uc[2]*tm[21]+uc[4]*tm[22]+uc[7]*tm[23]+uc[11]*tm[24];
    v[2] = uc[3] *tm[20]+uc[4]*tm[21]+uc[5]*tm[22]+uc[8]*tm[23]+uc[12]*tm[24];
    v[3] = uc[6] *tm[20]+uc[7]*tm[21]+uc[8]*tm[22]+uc[9]*tm[23]+uc[13]*tm[24];
    v[4] = uc[10]*tm[20]+uc[11]*tm[21]+uc[12]*tm[22]+uc[13]*tm[23]+uc[14]*tm[24];

    pc[14]= tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];

    // Store pc
    pc[0].storea(pc_p + 0*W);
    pc[1].storea(pc_p + 1*W);
    pc[2].storea(pc_p + 2*W);
    pc[3].storea(pc_p + 3*W);
    pc[4].storea(pc_p + 4*W);
    pc[5].storea(pc_p + 5*W);
    pc[6].storea(pc_p + 6*W);
    pc[7].storea(pc_p + 7*W);
    pc[8].storea(pc_p + 8*W);
    pc[9].storea(pc_p + 9*W);
    pc[10].storea(pc_p + 10*W);
    pc[11].storea(pc_p + 11*W);
    pc[12].storea(pc_p + 12*W);
    pc[13].storea(pc_p + 13*W);
    pc[14].storea(pc_p + 14*W);
  }

  /**
   * @brief      Vectorised update
   *
   * @param[in]  nodes_p        Nodes pointer
   * @param[in]  tracks_p       Tracks pointer
   * @param[in]  Xref_p         Reference parameters
   * @param[in]  H_p            Projection matrix
   * @param[in]  refResidual_p  Residual of reference
   * @param[in]  errorMeas_p    Measurement error squared
   * @param[in]  us_p           Updated state
   * @param[in]  uc_p           Updated covariance
   * @param[in]  chi2_p         Chi2
   */
  static inline void update (
    const std::array<Sch::Item, W>& nodes_p,
    const std::vector<VectorFit::Track>& tracks_p,
    fp_ptr_64_const Xref_p,
    fp_ptr_64_const H_p,
    fp_ptr_64_const refResidual_p,
    fp_ptr_64_const errorMeas_p,
    fp_ptr_64 us_p,
    fp_ptr_64 uc_p,
    fp_ptr_64 chi2_p
  )  {
    using vectype = typename Vectype<W>::type;
    using boolvectype = typename Vectype<W>::booltype;

    std::array<vectype, 15> uc;
    std::array<vectype, 5> us;
    std::array<vectype, 5> Xref;
    std::array<vectype, 5> H;
    std::array<vectype, 5> cht;
    vectype res, refResidual, errorMeas, chi2;

    // Load predicted state
    us[0].loada(us_p + 0*W);
    us[1].loada(us_p + 1*W);
    us[2].loada(us_p + 2*W);
    us[3].loada(us_p + 3*W);
    us[4].loada(us_p + 4*W);

    // Load predicted covariance
    uc[0].loada(uc_p + 0*W);
    uc[1].loada(uc_p + 1*W);
    uc[2].loada(uc_p + 2*W);
    uc[3].loada(uc_p + 3*W);
    uc[4].loada(uc_p + 4*W);
    uc[5].loada(uc_p + 5*W);
    uc[6].loada(uc_p + 6*W);
    uc[7].loada(uc_p + 7*W);
    uc[8].loada(uc_p + 8*W);
    uc[9].loada(uc_p + 9*W);
    uc[10].loada(uc_p + 10*W);
    uc[11].loada(uc_p + 11*W);
    uc[12].loada(uc_p + 12*W);
    uc[13].loada(uc_p + 13*W);
    uc[14].loada(uc_p + 14*W);

    // Load Xref
    Xref[0].loada(Xref_p + 0*W);
    Xref[1].loada(Xref_p + 1*W);
    Xref[2].loada(Xref_p + 2*W);
    Xref[3].loada(Xref_p + 3*W);
    Xref[4].loada(Xref_p + 4*W);

    // Load H
    H[0].loada(H_p + 0*W);
    H[1].loada(H_p + 1*W);
    H[2].loada(H_p + 2*W);
    H[3].loada(H_p + 3*W);
    H[4].loada(H_p + 4*W);

    // load vrefResidual
    refResidual.loada(refResidual_p);

    // load verrorMeas2
    errorMeas.loada(errorMeas_p);

    res = refResidual
      +  H[0] * (Xref[0] - us[0])
      +  H[1] * (Xref[1] - us[1])
      +  H[2] * (Xref[2] - us[2])
      +  H[3] * (Xref[3] - us[3])
      +  H[4] * (Xref[4] - us[4]);

    cht[0] = uc[0] *H[0] + uc[1] *H[1] + uc[3] *H[2] + uc[6] *H[3] + uc[10]*H[4];
    cht[1] = uc[1] *H[0] + uc[2] *H[1] + uc[4] *H[2] + uc[7] *H[3] + uc[11]*H[4];
    cht[2] = uc[3] *H[0] + uc[4] *H[1] + uc[5] *H[2] + uc[8] *H[3] + uc[12]*H[4];
    cht[3] = uc[6] *H[0] + uc[7] *H[1] + uc[8] *H[2] + uc[9] *H[3] + uc[13]*H[4];
    cht[4] = uc[10]*H[0] + uc[11]*H[1] + uc[12]*H[2] + uc[13]*H[3] + uc[14]*H[4];

    const boolvectype maskedVector = makeBoolHitOnTrack(nodes_p, tracks_p, std::make_index_sequence<W>());
    vectype errorResInv = 0;
    errorResInv.adda(maskedVector,
      ((PRECISION) 1.0) / (errorMeas * errorMeas
        + H[0] * cht[0]
        + H[1] * cht[1]
        + H[2] * cht[2]
        + H[3] * cht[3]
        + H[4] * cht[4]
    ));


    // update the state vector and cov matrix
    const vectype w = res * errorResInv;

    us[0] += cht[0] * w;
    us[1] += cht[1] * w;
    us[2] += cht[2] * w;
    us[3] += cht[3] * w;
    us[4] += cht[4] * w;

    uc[0]  -= errorResInv * cht[0] * cht[0];
    uc[1]  -= errorResInv * cht[1] * cht[0];
    uc[2]  -= errorResInv * cht[1] * cht[1];
    uc[3]  -= errorResInv * cht[2] * cht[0];
    uc[4]  -= errorResInv * cht[2] * cht[1];
    uc[5]  -= errorResInv * cht[2] * cht[2];
    uc[6]  -= errorResInv * cht[3] * cht[0];
    uc[7]  -= errorResInv * cht[3] * cht[1];
    uc[8]  -= errorResInv * cht[3] * cht[2];
    uc[9]  -= errorResInv * cht[3] * cht[3];
    uc[10] -= errorResInv * cht[4] * cht[0];
    uc[11] -= errorResInv * cht[4] * cht[1];
    uc[12] -= errorResInv * cht[4] * cht[2];
    uc[13] -= errorResInv * cht[4] * cht[3];
    uc[14] -= errorResInv * cht[4] * cht[4];

    chi2 = res * res * errorResInv;

    // Store uc
    uc[0].storea(uc_p + 0*W);
    uc[1].storea(uc_p + 1*W);
    uc[2].storea(uc_p + 2*W);
    uc[3].storea(uc_p + 3*W);
    uc[4].storea(uc_p + 4*W);
    uc[5].storea(uc_p + 5*W);
    uc[6].storea(uc_p + 6*W);
    uc[7].storea(uc_p + 7*W);
    uc[8].storea(uc_p + 8*W);
    uc[9].storea(uc_p + 9*W);
    uc[10].storea(uc_p + 10*W);
    uc[11].storea(uc_p + 11*W);
    uc[12].storea(uc_p + 12*W);
    uc[13].storea(uc_p + 13*W);
    uc[14].storea(uc_p + 14*W);

    // Store us
    us[0].storea(us_p + 0*W);
    us[1].storea(us_p + 1*W);
    us[2].storea(us_p + 2*W);
    us[3].storea(us_p + 3*W);
    us[4].storea(us_p + 4*W);

    // Store chi2
    chi2.storea(chi2_p);
  }

  /**
   * @brief      Vectorised update
   *             with explicit ps and pc
   */
  static inline void update (
    const std::array<Sch::Item, W>& nodes_p,
    const std::vector<VectorFit::Track>& tracks_p,
    fp_ptr_64_const ps_p,
    fp_ptr_64_const pc_p,
    fp_ptr_64_const Xref_p,
    fp_ptr_64_const H_p,
    fp_ptr_64_const refResidual_p,
    fp_ptr_64_const errorMeas_p,
    fp_ptr_64 us_p,
    fp_ptr_64 uc_p,
    fp_ptr_64 chi2_p
  )  {
    using vectype = typename Vectype<W>::type;
    using boolvectype = typename Vectype<W>::booltype;

    std::array<vectype, 15> uc;
    std::array<vectype, 5> us;
    std::array<vectype, 5> Xref;
    std::array<vectype, 5> H;
    std::array<vectype, 5> cht;
    vectype res, refResidual, errorMeas, chi2;

    // Load predicted state
    us[0].loada(ps_p + 0*W);
    us[1].loada(ps_p + 1*W);
    us[2].loada(ps_p + 2*W);
    us[3].loada(ps_p + 3*W);
    us[4].loada(ps_p + 4*W);

    // Load predicted covariance
    uc[0].loada(pc_p + 0*W);
    uc[1].loada(pc_p + 1*W);
    uc[2].loada(pc_p + 2*W);
    uc[3].loada(pc_p + 3*W);
    uc[4].loada(pc_p + 4*W);
    uc[5].loada(pc_p + 5*W);
    uc[6].loada(pc_p + 6*W);
    uc[7].loada(pc_p + 7*W);
    uc[8].loada(pc_p + 8*W);
    uc[9].loada(pc_p + 9*W);
    uc[10].loada(pc_p + 10*W);
    uc[11].loada(pc_p + 11*W);
    uc[12].loada(pc_p + 12*W);
    uc[13].loada(pc_p + 13*W);
    uc[14].loada(pc_p + 14*W);

    // Load Xref
    Xref[0].loada(Xref_p + 0*W);
    Xref[1].loada(Xref_p + 1*W);
    Xref[2].loada(Xref_p + 2*W);
    Xref[3].loada(Xref_p + 3*W);
    Xref[4].loada(Xref_p + 4*W);

    // Load H
    H[0].loada(H_p + 0*W);
    H[1].loada(H_p + 1*W);
    H[2].loada(H_p + 2*W);
    H[3].loada(H_p + 3*W);
    H[4].loada(H_p + 4*W);

    // load vrefResidual
    refResidual.loada(refResidual_p);

    // load verrorMeas2
    errorMeas.loada(errorMeas_p);

    res = refResidual
      +  H[0] * (Xref[0] - us[0])
      +  H[1] * (Xref[1] - us[1])
      +  H[2] * (Xref[2] - us[2])
      +  H[3] * (Xref[3] - us[3])
      +  H[4] * (Xref[4] - us[4]);

    cht[0] = uc[0] *H[0] + uc[1] *H[1] + uc[3] *H[2] + uc[6] *H[3] + uc[10]*H[4];
    cht[1] = uc[1] *H[0] + uc[2] *H[1] + uc[4] *H[2] + uc[7] *H[3] + uc[11]*H[4];
    cht[2] = uc[3] *H[0] + uc[4] *H[1] + uc[5] *H[2] + uc[8] *H[3] + uc[12]*H[4];
    cht[3] = uc[6] *H[0] + uc[7] *H[1] + uc[8] *H[2] + uc[9] *H[3] + uc[13]*H[4];
    cht[4] = uc[10]*H[0] + uc[11]*H[1] + uc[12]*H[2] + uc[13]*H[3] + uc[14]*H[4];

    const boolvectype maskedVector = makeBoolHitOnTrack(nodes_p, tracks_p, std::make_index_sequence<W>());
    vectype errorResInv = 0;
    errorResInv.adda(maskedVector,
      ((PRECISION) 1.0) / (errorMeas * errorMeas
        + H[0] * cht[0]
        + H[1] * cht[1]
        + H[2] * cht[2]
        + H[3] * cht[3]
        + H[4] * cht[4]
    ));


    // update the state vector and cov matrix
    const vectype w = res * errorResInv;

    us[0] += cht[0] * w;
    us[1] += cht[1] * w;
    us[2] += cht[2] * w;
    us[3] += cht[3] * w;
    us[4] += cht[4] * w;

    uc[0]  -= errorResInv * cht[0] * cht[0];
    uc[1]  -= errorResInv * cht[1] * cht[0];
    uc[2]  -= errorResInv * cht[1] * cht[1];
    uc[3]  -= errorResInv * cht[2] * cht[0];
    uc[4]  -= errorResInv * cht[2] * cht[1];
    uc[5]  -= errorResInv * cht[2] * cht[2];
    uc[6]  -= errorResInv * cht[3] * cht[0];
    uc[7]  -= errorResInv * cht[3] * cht[1];
    uc[8]  -= errorResInv * cht[3] * cht[2];
    uc[9]  -= errorResInv * cht[3] * cht[3];
    uc[10] -= errorResInv * cht[4] * cht[0];
    uc[11] -= errorResInv * cht[4] * cht[1];
    uc[12] -= errorResInv * cht[4] * cht[2];
    uc[13] -= errorResInv * cht[4] * cht[3];
    uc[14] -= errorResInv * cht[4] * cht[4];

    chi2 = res * res * errorResInv;

    // Store uc
    uc[0].storea(uc_p + 0*W);
    uc[1].storea(uc_p + 1*W);
    uc[2].storea(uc_p + 2*W);
    uc[3].storea(uc_p + 3*W);
    uc[4].storea(uc_p + 4*W);
    uc[5].storea(uc_p + 5*W);
    uc[6].storea(uc_p + 6*W);
    uc[7].storea(uc_p + 7*W);
    uc[8].storea(uc_p + 8*W);
    uc[9].storea(uc_p + 9*W);
    uc[10].storea(uc_p + 10*W);
    uc[11].storea(uc_p + 11*W);
    uc[12].storea(uc_p + 12*W);
    uc[13].storea(uc_p + 13*W);
    uc[14].storea(uc_p + 14*W);

    // Store us
    us[0].storea(us_p + 0*W);
    us[1].storea(us_p + 1*W);
    us[2].storea(us_p + 2*W);
    us[3].storea(us_p + 3*W);
    us[4].storea(us_p + 4*W);

    // Store chi2
    chi2.storea(chi2_p);
  }

  /**
   * @brief      Smoother average function
   *
   * @param[in]  X1_p  State one
   * @param[in]  X2_p  State two
   * @param[in]  C1_p  Covariance one
   * @param[in]  C2_p  Covariance two
   * @param[in]  X_p   Average state
   * @param[in]  C_p   Average covariance
   *
   * @return     Bitmask with 1 for each success average,
   *             0 for each failed average. The average
   *             function is the only one that can fail,
   *             due to accumulated errors in division.
   */
  static inline uint16_t average (
    fp_ptr_64_const X1_p,
    fp_ptr_64_const X2_p,
    fp_ptr_64_const C1_p,
    fp_ptr_64_const C2_p,
    fp_ptr_64 X_p,
    fp_ptr_64 C_p
  ) {
    using vectype = typename Vectype<W>::type;
    using boolvectype = typename Vectype<W>::booltype;

    std::array<vectype, 25> K;
    std::array<vectype, 5> X1;
    std::array<vectype, 5> X2;
    std::array<vectype, 15> C1;
    std::array<vectype, 15> C2;
    std::array<vectype, 15> Csum;
    std::array<vectype, 5> X;
    std::array<vectype, 15> C;
    std::array<vectype, 15> L;
    std::array<vectype, 15> inv;
    std::array<vectype, 5> diff;

    boolvectype success (true);

    // Load X1
    X1[0].loada(X1_p + 0*W);
    X1[1].loada(X1_p + 1*W);
    X1[2].loada(X1_p + 2*W);
    X1[3].loada(X1_p + 3*W);
    X1[4].loada(X1_p + 4*W);

    // Load X2
    X2[0].loada(X2_p + 0*W);
    X2[1].loada(X2_p + 1*W);
    X2[2].loada(X2_p + 2*W);
    X2[3].loada(X2_p + 3*W);
    X2[4].loada(X2_p + 4*W);

    // Load C1
    C1[0].loada(C1_p + 0*W);
    C1[1].loada(C1_p + 1*W);
    C1[2].loada(C1_p + 2*W);
    C1[3].loada(C1_p + 3*W);
    C1[4].loada(C1_p + 4*W);
    C1[5].loada(C1_p + 5*W);
    C1[6].loada(C1_p + 6*W);
    C1[7].loada(C1_p + 7*W);
    C1[8].loada(C1_p + 8*W);
    C1[9].loada(C1_p + 9*W);
    C1[10].loada(C1_p + 10*W);
    C1[11].loada(C1_p + 11*W);
    C1[12].loada(C1_p + 12*W);
    C1[13].loada(C1_p + 13*W);
    C1[14].loada(C1_p + 14*W);

    // Load C2
    C2[0].loada(C2_p + 0*W);
    C2[1].loada(C2_p + 1*W);
    C2[2].loada(C2_p + 2*W);
    C2[3].loada(C2_p + 3*W);
    C2[4].loada(C2_p + 4*W);
    C2[5].loada(C2_p + 5*W);
    C2[6].loada(C2_p + 6*W);
    C2[7].loada(C2_p + 7*W);
    C2[8].loada(C2_p + 8*W);
    C2[9].loada(C2_p + 9*W);
    C2[10].loada(C2_p + 10*W);
    C2[11].loada(C2_p + 11*W);
    C2[12].loada(C2_p + 12*W);
    C2[13].loada(C2_p + 13*W);
    C2[14].loada(C2_p + 14*W);

    // C sum
    Csum[0]  = C1[0]  + C2[0];
    Csum[1]  = C1[1]  + C2[1];
    Csum[2]  = C1[2]  + C2[2];
    Csum[3]  = C1[3]  + C2[3];
    Csum[4]  = C1[4]  + C2[4];
    Csum[5]  = C1[5]  + C2[5];
    Csum[6]  = C1[6]  + C2[6];
    Csum[7]  = C1[7]  + C2[7];
    Csum[8]  = C1[8]  + C2[8];
    Csum[9]  = C1[9]  + C2[9];
    Csum[10] = C1[10] + C2[10];
    Csum[11] = C1[11] + C2[11];
    Csum[12] = C1[12] + C2[12];
    Csum[13] = C1[13] + C2[13];
    Csum[14] = C1[14] + C2[14];

    // Factorization
    L[0] = Csum[0];
    success &= L[0] > 0.0;
    L[0] = ((PRECISION) 1.0) / L[0];
    L[0] = L[0].sqrt();

    L[1]  = Csum[1]  * L[0];
    L[3]  = Csum[3]  * L[0];
    L[6]  = Csum[6]  * L[0];
    L[10] = Csum[10] * L[0];

    L[2] = Csum[2] - L[1]*L[1];
    success &= L[2] > 0.0;
    L[2] = ((PRECISION) 1.0) / L[2];
    L[2] = L[2].sqrt();

    L[4]  = (Csum[4]  - L[3] *L[1]) * L[2];
    L[7]  = (Csum[7]  - L[6] *L[1]) * L[2];
    L[11] = (Csum[11] - L[10]*L[1]) * L[2];

    L[5] = Csum[5] - L[3]*L[3] - L[4]*L[4];
    success &= L[5] > 0.0;
    L[5] = ((PRECISION) 1.0) / L[5];
    L[5] = L[5].sqrt();

    L[8]  = (Csum[8]  - L[6] *L[3] - L[7] *L[4]) * L[5];
    L[12] = (Csum[12] - L[10]*L[3] - L[11]*L[4]) * L[5];

    L[9] = Csum[9] - L[6]*L[6] - L[7]*L[7] - L[8]*L[8];
    success &= L[9] > 0.0;
    L[9] = ((PRECISION) 1.0) / L[9];
    L[9] = L[9].sqrt();

    L[13] = (Csum[13] - L[10]*L[6] - L[11]*L[7] - L[12]*L[8]) * L[9];

    L[14] = Csum[14] - L[10]*L[10] - L[11]*L[11] - L[12]*L[12] - L[13]*L[13];
    success &= L[14] > 0.0;
    L[14] = ((PRECISION) 1.0) / L[14];
    L[14] = L[14].sqrt();


    // Inversion

    // Forward substitution
    inv[0]  = L[0];
    inv[1]  = (- L[1] *inv[0]) * L[2];
    inv[3]  = (- L[3] *inv[0] - L[4] *inv[1]) * L[5];
    inv[6]  = (- L[6] *inv[0] - L[7] *inv[1] - L[8] *inv[3]) * L[9];
    inv[10] = (- L[10]*inv[0] - L[11]*inv[1] - L[12]*inv[3] - L[13]*inv[6]) * L[14];

    inv[2]  = L[2];
    inv[4]  = (- L[4] *inv[2]) * L[5];
    inv[7]  = (- L[7] *inv[2] - L[8] *inv[4]) * L[9];
    inv[11] = (- L[11]*inv[2] - L[12]*inv[4] - L[13]*inv[7]) * L[14];

    inv[5]  = L[5];
    inv[8]  = (- L[8] *inv[5]) * L[9];
    inv[12] = (- L[12]*inv[5] - L[13]*inv[8]) * L[14];

    inv[9]  = L[9];
    inv[13] = (- L[13]*inv[9]) * L[14];

    inv[14] = L[14];

    // Backward substitution
    inv[10] = inv[10] * L[14];
    inv[6]  = (inv[6] - L[13]*inv[10]) * L[9];
    inv[3]  = (inv[3] - L[12]*inv[10]  - L[8]*inv[6]) * L[5];
    inv[1]  = (inv[1] - L[11]*inv[10]  - L[7]*inv[6]  - L[4]*inv[3]) * L[2];
    inv[0]  = (inv[0] - L[10]*inv[10]  - L[6]*inv[6]  - L[3]*inv[3]  - L[1]*inv[1]) * L[0];

    inv[11] = inv[11] * L[14];
    inv[7]  = (inv[7] - L[13]*inv[11]) * L[9];
    inv[4]  = (inv[4] - L[12]*inv[11]  - L[8]*inv[7]) * L[5];
    inv[2]  = (inv[2] - L[11]*inv[11]  - L[7]*inv[7]  - L[4]*inv[4]) * L[2];

    inv[12] = inv[12] * L[14];
    inv[8]  = (inv[8] - L[13]*inv[12]) * L[9];
    inv[5]  = (inv[5] - L[12]*inv[12]  - L[8]*inv[8]) * L[5];

    inv[13] = inv[13] * L[14];
    inv[9]  = (inv[9] - L[13]*inv[13]) * L[9];

    inv[14] = inv[14] * L[14];

    K[0]  = C1[0] *inv[0]  + C1[1] *inv[1]  + C1[3] *inv[3]  + C1[6] *inv[6]  + C1[10]*inv[10];
    K[1]  = C1[0] *inv[1]  + C1[1] *inv[2]  + C1[3] *inv[4]  + C1[6] *inv[7]  + C1[10]*inv[11];
    K[2]  = C1[0] *inv[3]  + C1[1] *inv[4]  + C1[3] *inv[5]  + C1[6] *inv[8]  + C1[10]*inv[12];
    K[3]  = C1[0] *inv[6]  + C1[1] *inv[7]  + C1[3] *inv[8]  + C1[6] *inv[9]  + C1[10]*inv[13];
    K[4]  = C1[0] *inv[10] + C1[1] *inv[11] + C1[3] *inv[12] + C1[6] *inv[13] + C1[10]*inv[14];
    K[5]  = C1[1] *inv[0]  + C1[2] *inv[1]  + C1[4] *inv[3]  + C1[7] *inv[6]  + C1[11]*inv[10];
    K[6]  = C1[1] *inv[1]  + C1[2] *inv[2]  + C1[4] *inv[4]  + C1[7] *inv[7]  + C1[11]*inv[11];
    K[7]  = C1[1] *inv[3]  + C1[2] *inv[4]  + C1[4] *inv[5]  + C1[7] *inv[8]  + C1[11]*inv[12];
    K[8]  = C1[1] *inv[6]  + C1[2] *inv[7]  + C1[4] *inv[8]  + C1[7] *inv[9]  + C1[11]*inv[13];
    K[9]  = C1[1] *inv[10] + C1[2] *inv[11] + C1[4] *inv[12] + C1[7] *inv[13] + C1[11]*inv[14];
    K[10] = C1[3] *inv[0]  + C1[4] *inv[1]  + C1[5] *inv[3]  + C1[8] *inv[6]  + C1[12]*inv[10];
    K[11] = C1[3] *inv[1]  + C1[4] *inv[2]  + C1[5] *inv[4]  + C1[8] *inv[7]  + C1[12]*inv[11];
    K[12] = C1[3] *inv[3]  + C1[4] *inv[4]  + C1[5] *inv[5]  + C1[8] *inv[8]  + C1[12]*inv[12];
    K[13] = C1[3] *inv[6]  + C1[4] *inv[7]  + C1[5] *inv[8]  + C1[8] *inv[9]  + C1[12]*inv[13];
    K[14] = C1[3] *inv[10] + C1[4] *inv[11] + C1[5] *inv[12] + C1[8] *inv[13] + C1[12]*inv[14];
    K[15] = C1[6] *inv[0]  + C1[7] *inv[1]  + C1[8] *inv[3]  + C1[9] *inv[6]  + C1[13]*inv[10];
    K[16] = C1[6] *inv[1]  + C1[7] *inv[2]  + C1[8] *inv[4]  + C1[9] *inv[7]  + C1[13]*inv[11];
    K[17] = C1[6] *inv[3]  + C1[7] *inv[4]  + C1[8] *inv[5]  + C1[9] *inv[8]  + C1[13]*inv[12];
    K[18] = C1[6] *inv[6]  + C1[7] *inv[7]  + C1[8] *inv[8]  + C1[9] *inv[9]  + C1[13]*inv[13];
    K[19] = C1[6] *inv[10] + C1[7] *inv[11] + C1[8] *inv[12] + C1[9] *inv[13] + C1[13]*inv[14];
    K[20] = C1[10]*inv[0]  + C1[11]*inv[1]  + C1[12]*inv[3]  + C1[13]*inv[6]  + C1[14]*inv[10];
    K[21] = C1[10]*inv[1]  + C1[11]*inv[2]  + C1[12]*inv[4]  + C1[13]*inv[7]  + C1[14]*inv[11];
    K[22] = C1[10]*inv[3]  + C1[11]*inv[4]  + C1[12]*inv[5]  + C1[13]*inv[8]  + C1[14]*inv[12];
    K[23] = C1[10]*inv[6]  + C1[11]*inv[7]  + C1[12]*inv[8]  + C1[13]*inv[9]  + C1[14]*inv[13];
    K[24] = C1[10]*inv[10] + C1[11]*inv[11] + C1[12]*inv[12] + C1[13]*inv[13] + C1[14]*inv[14];

    // X <- X1 + C1*inverse(C1+C2)*(X2-X1) =  X1 + K*(X2-X1) = X1 + K*d
    diff[0] = X2[0] - X1[0];
    diff[1] = X2[1] - X1[1];
    diff[2] = X2[2] - X1[2];
    diff[3] = X2[3] - X1[3];
    diff[4] = X2[4] - X1[4];

    X[0] = X1[0] + K[0] *diff[0] + K[1] *diff[1] + K[2] *diff[2] + K[3] *diff[3] + K[4] *diff[4];
    X[1] = X1[1] + K[5] *diff[0] + K[6] *diff[1] + K[7] *diff[2] + K[8] *diff[3] + K[9] *diff[4];
    X[2] = X1[2] + K[10]*diff[0] + K[11]*diff[1] + K[12]*diff[2] + K[13]*diff[3] + K[14]*diff[4];
    X[3] = X1[3] + K[15]*diff[0] + K[16]*diff[1] + K[17]*diff[2] + K[18]*diff[3] + K[19]*diff[4];
    X[4] = X1[4] + K[20]*diff[0] + K[21]*diff[1] + K[22]*diff[2] + K[23]*diff[3] + K[24]*diff[4];

    // C <-  C1 * inverse(C1+C2)  * C2 =  K * C2
    C[0]  = K[0] *C2[0]  + K[1] *C2[1]  + K[2] *C2[3]  + K[3] *C2[6]  + K[4] *C2[10];
    C[1]  = K[5] *C2[0]  + K[6] *C2[1]  + K[7] *C2[3]  + K[8] *C2[6]  + K[9] *C2[10];
    C[2]  = K[5] *C2[1]  + K[6] *C2[2]  + K[7] *C2[4]  + K[8] *C2[7]  + K[9] *C2[11];
    C[3]  = K[10]*C2[0]  + K[11]*C2[1]  + K[12]*C2[3]  + K[13]*C2[6]  + K[14]*C2[10];
    C[4]  = K[10]*C2[1]  + K[11]*C2[2]  + K[12]*C2[4]  + K[13]*C2[7]  + K[14]*C2[11];
    C[5]  = K[10]*C2[3]  + K[11]*C2[4]  + K[12]*C2[5]  + K[13]*C2[8]  + K[14]*C2[12];
    C[6]  = K[15]*C2[0]  + K[16]*C2[1]  + K[17]*C2[3]  + K[18]*C2[6]  + K[19]*C2[10];
    C[7]  = K[15]*C2[1]  + K[16]*C2[2]  + K[17]*C2[4]  + K[18]*C2[7]  + K[19]*C2[11];
    C[8]  = K[15]*C2[3]  + K[16]*C2[4]  + K[17]*C2[5]  + K[18]*C2[8]  + K[19]*C2[12];
    C[9]  = K[15]*C2[6]  + K[16]*C2[7]  + K[17]*C2[8]  + K[18]*C2[9]  + K[19]*C2[13];
    C[10] = K[20]*C2[0]  + K[21]*C2[1]  + K[22]*C2[3]  + K[23]*C2[6]  + K[24]*C2[10]; 
    C[11] = K[20]*C2[1]  + K[21]*C2[2]  + K[22]*C2[4]  + K[23]*C2[7]  + K[24]*C2[11]; 
    C[12] = K[20]*C2[3]  + K[21]*C2[4]  + K[22]*C2[5]  + K[23]*C2[8]  + K[24]*C2[12]; 
    C[13] = K[20]*C2[6]  + K[21]*C2[7]  + K[22]*C2[8]  + K[23]*C2[9]  + K[24]*C2[13]; 
    C[14] = K[20]*C2[10] + K[21]*C2[11] + K[22]*C2[12] + K[23]*C2[13] + K[24]*C2[14];

    // Store X
    X[0].storea(X_p + 0*W);
    X[1].storea(X_p + 1*W);
    X[2].storea(X_p + 2*W);
    X[3].storea(X_p + 3*W);
    X[4].storea(X_p + 4*W);

    // Store C
    C[0].storea(C_p + 0*W);
    C[1].storea(C_p + 1*W);
    C[2].storea(C_p + 2*W);
    C[3].storea(C_p + 3*W);
    C[4].storea(C_p + 4*W);
    C[5].storea(C_p + 5*W);
    C[6].storea(C_p + 6*W);
    C[7].storea(C_p + 7*W);
    C[8].storea(C_p + 8*W);
    C[9].storea(C_p + 9*W);
    C[10].storea(C_p + 10*W);
    C[11].storea(C_p + 11*W);
    C[12].storea(C_p + 12*W);
    C[13].storea(C_p + 13*W);
    C[14].storea(C_p + 14*W);

    //return to_bits(success);
    return true;
  }

  static inline void updateResiduals (
    const std::array<Sch::Item, W>& nodes_p,
    const std::vector<VectorFit::Track>& tracks_p,
    fp_ptr_64_const pa_p,
    fp_ptr_64_const pm_p,
    fp_ptr_64_const rr_p,
    fp_ptr_64_const em_p,
    fp_ptr_64_const ss_p,
    fp_ptr_64_const sc_p,
    fp_ptr_64 res_p,
    fp_ptr_64 errRes_p
  ) {
    using vectype = typename Vectype<W>::type;
    using boolvectype = typename Vectype<W>::booltype;

    std::array<vectype, 5> pm;
    std::array<vectype, 5> pa;
    std::array<vectype, 5> ss;
    std::array<vectype, 15> sc;
    std::array<vectype, 5> v;
    vectype res, errRes, em, rr, HCH, sign, value, error;


    // Load pm
    pm[0].loada(pm_p + 0*W);
    pm[1].loada(pm_p + 1*W);
    pm[2].loada(pm_p + 2*W);
    pm[3].loada(pm_p + 3*W);
    pm[4].loada(pm_p + 4*W);

    // Load pa
    pa[0].loada(pa_p + 0*W);
    pa[1].loada(pa_p + 1*W);
    pa[2].loada(pa_p + 2*W);
    pa[3].loada(pa_p + 3*W);
    pa[4].loada(pa_p + 4*W);

    // Load ss
    ss[0].loada(ss_p + 0*W);
    ss[1].loada(ss_p + 1*W);
    ss[2].loada(ss_p + 2*W);
    ss[3].loada(ss_p + 3*W);
    ss[4].loada(ss_p + 4*W);

    // Load sc
    sc[0].loada(sc_p + 0*W);
    sc[1].loada(sc_p + 1*W);
    sc[2].loada(sc_p + 2*W);
    sc[3].loada(sc_p + 3*W);
    sc[4].loada(sc_p + 4*W);
    sc[5].loada(sc_p + 5*W);
    sc[6].loada(sc_p + 6*W);
    sc[7].loada(sc_p + 7*W);
    sc[8].loada(sc_p + 8*W);
    sc[9].loada(sc_p + 9*W);
    sc[10].loada(sc_p + 10*W);
    sc[11].loada(sc_p + 11*W);
    sc[12].loada(sc_p + 12*W);
    sc[13].loada(sc_p + 13*W);
    sc[14].loada(sc_p + 14*W);

    // Load em
    em.loada(em_p);

    // Load rr
    rr.loada(rr_p);

    // HCH = pm[ 0]*_0 + pm[ 1]*_1 + pm[ 2]*_2 + pm[ 3]*_3 + pm[ 4]*_4;
    v[0] = sc[0] *pm[0]+sc[1] *pm[1]+sc[3] *pm[2]+sc[6] *pm[3]+sc[10]*pm[4];
    v[1] = sc[1] *pm[0]+sc[2] *pm[1]+sc[4] *pm[2]+sc[7] *pm[3]+sc[11]*pm[4];
    v[2] = sc[3] *pm[0]+sc[4] *pm[1]+sc[5] *pm[2]+sc[8] *pm[3]+sc[12]*pm[4];
    v[3] = sc[6] *pm[0]+sc[7] *pm[1]+sc[8] *pm[2]+sc[9] *pm[3]+sc[13]*pm[4];
    v[4] = sc[10]*pm[0]+sc[11]*pm[1]+sc[12]*pm[2]+sc[13]*pm[3]+sc[14]*pm[4];
    HCH = pm[0]*v[0] + pm[1]*v[1] + pm[2]*v[2] + pm[3]*v[3] + pm[4]*v[4];

    // const PRECISION V = node.m_errMeasure * node.m_errMeasure;
    // const PRECISION sign = node.m_type == HitOnTrack ? -1 : 1;
    const boolvectype maskedHitOnTrack = makeBoolHitOnTrack(nodes_p, tracks_p, std::make_index_sequence<W>());
    sign = ((PRECISION) 1.0);
    sign.adda(maskedHitOnTrack, ((PRECISION) -2.0));

    // const TrackVectorContiguous& refX = node.m_refVector.m_parameters;
    // value = node.m_refResidual + (pm * (pa - node.getSmooth<Op::StateVector>()));
    value = rr + (
        pm[0] * (pa[0] - ss[0]) +
        pm[1] * (pa[1] - ss[1]) +
        pm[2] * (pa[2] - ss[2]) +
        pm[3] * (pa[3] - ss[3]) +
        pm[4] * (pa[4] - ss[4])
    );

    // error = V + sign * HCH;
    error = em * em + sign * HCH;

    // Bring back the changes
    res    = 0.0;
    errRes = 0.0;

    res.adda(maskedHitOnTrack, value);
    errRes.adda(maskedHitOnTrack, error);

    res.storea(res_p);
    errRes.storea(errRes_p);

  }
};

template<class T>
struct Math {
  template<unsigned W>
  static inline void predictState (
    fp_ptr_64_const tm,
    fp_ptr_64_const tv,
    fp_ptr_64_const us,
    fp_ptr_64 ps
  );
};

template<>
struct Math<Op::Forward> {
  template<unsigned W>
  static inline void predictState (
    fp_ptr_64_const tm_p,
    fp_ptr_64_const tv_p,
    fp_ptr_64_const us_p,
    fp_ptr_64 ps_p
  ) {
    using vectype = typename Vectype<W>::type;

    std::array<vectype, 25> tm;
    std::array<vectype, 5> tv;
    std::array<vectype, 5> us;
    std::array<vectype, 5> ps;

    // Load tm
    tm[0].loada(tm_p + 0*W);
    tm[1].loada(tm_p + 1*W);
    tm[2].loada(tm_p + 2*W);
    tm[3].loada(tm_p + 3*W);
    tm[4].loada(tm_p + 4*W);
    tm[5].loada(tm_p + 5*W);
    tm[6].loada(tm_p + 6*W);
    tm[7].loada(tm_p + 7*W);
    tm[8].loada(tm_p + 8*W);
    tm[9].loada(tm_p + 9*W);
    tm[10].loada(tm_p + 10*W);
    tm[11].loada(tm_p + 11*W);
    tm[12].loada(tm_p + 12*W);
    tm[13].loada(tm_p + 13*W);
    tm[14].loada(tm_p + 14*W);
    tm[15].loada(tm_p + 15*W);
    tm[16].loada(tm_p + 16*W);
    tm[17].loada(tm_p + 17*W);
    tm[18].loada(tm_p + 18*W);
    tm[19].loada(tm_p + 19*W);
    tm[20].loada(tm_p + 20*W);
    tm[21].loada(tm_p + 21*W);
    tm[22].loada(tm_p + 22*W);
    tm[23].loada(tm_p + 23*W);
    tm[24].loada(tm_p + 24*W);

    // Load tv
    tv[0].loada(tv_p + 0*W);
    tv[1].loada(tv_p + 1*W);
    tv[2].loada(tv_p + 2*W);
    tv[3].loada(tv_p + 3*W);
    tv[4].loada(tv_p + 4*W);

    // Load us
    us[0].loada(us_p + 0*W);
    us[1].loada(us_p + 1*W);
    us[2].loada(us_p + 2*W);
    us[3].loada(us_p + 3*W);
    us[4].loada(us_p + 4*W);

    ps[0] = tv[0] + tm[0]  * us[0] + tm[1]  * us[1] + tm[2]  * us[2] + tm[3]  * us[3] + tm[4]  * us[4];
    ps[1] = tv[1] + tm[5]  * us[0] + tm[6]  * us[1] + tm[7]  * us[2] + tm[8]  * us[3] + tm[9]  * us[4];
    ps[2] = tv[2] + tm[10] * us[0] + tm[11] * us[1] + tm[12] * us[2] + tm[13] * us[3] + tm[14] * us[4];
    ps[3] = tv[3] + tm[15] * us[0] + tm[16] * us[1] + tm[17] * us[2] + tm[18] * us[3] + tm[19] * us[4];
    ps[4] = tv[4] + tm[20] * us[0] + tm[21] * us[1] + tm[22] * us[2] + tm[23] * us[3] + tm[24] * us[4];

    // Store ps
    ps[0].storea(ps_p + 0*W);
    ps[1].storea(ps_p + 1*W);
    ps[2].storea(ps_p + 2*W);
    ps[3].storea(ps_p + 3*W);
    ps[4].storea(ps_p + 4*W);
  }

  template<unsigned W>
  static inline void predictCovariance (
    fp_ptr_64_const tm,
    fp_ptr_64_const nm,
    fp_ptr_64_const uc,
    fp_ptr_64 pc
  ) {
    // Delegate to similarity_5_5 implementation
    MathCommon<W>::similarity_5_5(tm, uc, pc);

    using vectype = typename Vectype<W>::type;
    (vectype().loada(pc +  0*W) + vectype().loada(nm +  0*W)).storea(pc +  0*W);
    (vectype().loada(pc +  1*W) + vectype().loada(nm +  1*W)).storea(pc +  1*W);
    (vectype().loada(pc +  2*W) + vectype().loada(nm +  2*W)).storea(pc +  2*W);
    (vectype().loada(pc +  3*W) + vectype().loada(nm +  3*W)).storea(pc +  3*W);
    (vectype().loada(pc +  4*W) + vectype().loada(nm +  4*W)).storea(pc +  4*W);
    (vectype().loada(pc +  5*W) + vectype().loada(nm +  5*W)).storea(pc +  5*W);
    (vectype().loada(pc +  6*W) + vectype().loada(nm +  6*W)).storea(pc +  6*W);
    (vectype().loada(pc +  7*W) + vectype().loada(nm +  7*W)).storea(pc +  7*W);
    (vectype().loada(pc +  8*W) + vectype().loada(nm +  8*W)).storea(pc +  8*W);
    (vectype().loada(pc +  9*W) + vectype().loada(nm +  9*W)).storea(pc +  9*W);
    (vectype().loada(pc + 10*W) + vectype().loada(nm + 10*W)).storea(pc + 10*W);
    (vectype().loada(pc + 11*W) + vectype().loada(nm + 11*W)).storea(pc + 11*W);
    (vectype().loada(pc + 12*W) + vectype().loada(nm + 12*W)).storea(pc + 12*W);
    (vectype().loada(pc + 13*W) + vectype().loada(nm + 13*W)).storea(pc + 13*W);
    (vectype().loada(pc + 14*W) + vectype().loada(nm + 14*W)).storea(pc + 14*W);
  }
};

template<>
struct Math<Op::Backward> {
  template<unsigned W>
  static inline void predictState (
    fp_ptr_64_const tm_p,
    fp_ptr_64_const tv_p,
    fp_ptr_64_const us_p,
    fp_ptr_64 ps_p
  ) {
    using vectype = typename Vectype<W>::type;

    std::array<vectype, 25> tm;
    std::array<vectype, 5> tv;
    std::array<vectype, 5> us;
    std::array<vectype, 5> ps;
    std::array<vectype, 5> s;

    // Load tm
    tm[0].loada(tm_p + 0*W);
    tm[1].loada(tm_p + 1*W);
    tm[2].loada(tm_p + 2*W);
    tm[3].loada(tm_p + 3*W);
    tm[4].loada(tm_p + 4*W);
    tm[5].loada(tm_p + 5*W);
    tm[6].loada(tm_p + 6*W);
    tm[7].loada(tm_p + 7*W);
    tm[8].loada(tm_p + 8*W);
    tm[9].loada(tm_p + 9*W);
    tm[10].loada(tm_p + 10*W);
    tm[11].loada(tm_p + 11*W);
    tm[12].loada(tm_p + 12*W);
    tm[13].loada(tm_p + 13*W);
    tm[14].loada(tm_p + 14*W);
    tm[15].loada(tm_p + 15*W);
    tm[16].loada(tm_p + 16*W);
    tm[17].loada(tm_p + 17*W);
    tm[18].loada(tm_p + 18*W);
    tm[19].loada(tm_p + 19*W);
    tm[20].loada(tm_p + 20*W);
    tm[21].loada(tm_p + 21*W);
    tm[22].loada(tm_p + 22*W);
    tm[23].loada(tm_p + 23*W);
    tm[24].loada(tm_p + 24*W);

    // Load tv
    tv[0].loada(tv_p + 0*W);
    tv[1].loada(tv_p + 1*W);
    tv[2].loada(tv_p + 2*W);
    tv[3].loada(tv_p + 3*W);
    tv[4].loada(tv_p + 4*W);

    // Load us
    us[0].loada(us_p + 0*W);
    us[1].loada(us_p + 1*W);
    us[2].loada(us_p + 2*W);
    us[3].loada(us_p + 3*W);
    us[4].loada(us_p + 4*W);

    s[0] = us[0] - tv[0];
    s[1] = us[1] - tv[1];
    s[2] = us[2] - tv[2];
    s[3] = us[3] - tv[3];
    s[4] = us[4] - tv[4];

    ps[0] = tm[0]  * s[0] + tm[1]  * s[1] + tm[2]  * s[2] + tm[3]  * s[3] + tm[4]  * s[4];
    ps[1] = tm[5]  * s[0] + tm[6]  * s[1] + tm[7]  * s[2] + tm[8]  * s[3] + tm[9]  * s[4];
    ps[2] = tm[10] * s[0] + tm[11] * s[1] + tm[12] * s[2] + tm[13] * s[3] + tm[14] * s[4];
    ps[3] = tm[15] * s[0] + tm[16] * s[1] + tm[17] * s[2] + tm[18] * s[3] + tm[19] * s[4];
    ps[4] = tm[20] * s[0] + tm[21] * s[1] + tm[22] * s[2] + tm[23] * s[3] + tm[24] * s[4];

    // Store ps
    ps[0].storea(ps_p + 0*W);
    ps[1].storea(ps_p + 1*W);
    ps[2].storea(ps_p + 2*W);
    ps[3].storea(ps_p + 3*W);
    ps[4].storea(ps_p + 4*W);
  }

  template<unsigned W>
  static inline void predictCovariance (
    fp_ptr_64_const tm,
    fp_ptr_64 nm,
    fp_ptr_64_const uc,
    fp_ptr_64 pc
  ) {
    using vectype = typename Vectype<W>::type;
    (vectype().loada(nm +  0*W) + vectype().loada(uc +  0*W)).storea(nm +  0*W);
    (vectype().loada(nm +  1*W) + vectype().loada(uc +  1*W)).storea(nm +  1*W);
    (vectype().loada(nm +  2*W) + vectype().loada(uc +  2*W)).storea(nm +  2*W);
    (vectype().loada(nm +  3*W) + vectype().loada(uc +  3*W)).storea(nm +  3*W);
    (vectype().loada(nm +  4*W) + vectype().loada(uc +  4*W)).storea(nm +  4*W);
    (vectype().loada(nm +  5*W) + vectype().loada(uc +  5*W)).storea(nm +  5*W);
    (vectype().loada(nm +  6*W) + vectype().loada(uc +  6*W)).storea(nm +  6*W);
    (vectype().loada(nm +  7*W) + vectype().loada(uc +  7*W)).storea(nm +  7*W);
    (vectype().loada(nm +  8*W) + vectype().loada(uc +  8*W)).storea(nm +  8*W);
    (vectype().loada(nm +  9*W) + vectype().loada(uc +  9*W)).storea(nm +  9*W);
    (vectype().loada(nm + 10*W) + vectype().loada(uc + 10*W)).storea(nm + 10*W);
    (vectype().loada(nm + 11*W) + vectype().loada(uc + 11*W)).storea(nm + 11*W);
    (vectype().loada(nm + 12*W) + vectype().loada(uc + 12*W)).storea(nm + 12*W);
    (vectype().loada(nm + 13*W) + vectype().loada(uc + 13*W)).storea(nm + 13*W);
    (vectype().loada(nm + 14*W) + vectype().loada(uc + 14*W)).storea(nm + 14*W);

    // Delegate to similarity_5_5 implementation
    MathCommon<W>::similarity_5_5(tm, nm, pc);
  }
};

}

}
