#pragma once

// Include vectorclass
// #define MAX_VECTOR_SIZE (SIZE_OF_PRECISION * VECTOR_WIDTH) doesnt work
#if defined(__AVX512F__) || defined (__AVX512__)
  #define MAX_VECTOR_SIZE 512
#elif defined(__AVX__)
  #define MAX_VECTOR_SIZE 256
#elif defined(__SSE__) || defined(__ALTIVEC__) || defined(__aarch64__)
  #define MAX_VECTOR_SIZE 128
#endif

#include "../../../vectorclass/vectorclass.h"


#ifdef SP

#if defined(__AVX512F__) || defined(__AVX512__)
  template<> struct Vectype<16> { using type = Vec16f; using booltype = Vec16fb; };
#endif
#if defined(__AVX__)
  template<> struct Vectype<8> { using type = Vec8f; using booltype = Vec8fb; };
#endif
#if defined(__SSE__) || defined(__ALTIVEC__) || defined(__aarch64__)
  template<> struct Vectype<4> { using type = Vec4f; using booltype = Vec4fb; };
#endif

#else

#if defined(__AVX512F__) || defined (__AVX512__)
  template<> struct Vectype<8> { using type = Vec8d; using booltype = Vec8db; };
#endif
#if defined(__AVX__)
  template<> struct Vectype<4> { using type = Vec4d; using booltype = Vec4db; };
#endif
#if defined(__SSE__) || defined(__ALTIVEC__) || defined(__aarch64__)
  template<> struct Vectype<2> { using type = Vec2d; using booltype = Vec2db; };
#endif

#endif
