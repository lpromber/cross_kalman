Cross architecture Kalman filter
================================

Welcome to the cross architecture Kalman filter project! However way you ended up here, we hope you enjoy your stay :)

The aim of this project is to produce a *fast* and *efficient* Kalman filter, while preserving *correctness* of results,
across a variety of architectures.

Compiling and running
---------------------

Getting the latest and greatest,

    git clone https://gitlab.cern.ch/dcampora/cross_kalman.git
    cd cross_kalman
    git clone https://github.com/edanor/umesimd.git
    wget https://lbevent.cern.ch/online/dcampora/kalman_filter/events.tar.gz
    tar zxvf events.tar.gz

Compiling and running,

    make
    ./cross_kalman

There are plenty of options available. Have a look at -h!

If you want to compile it for single precision, or even different vector lengths,
this is quite easy,

    # Compile with single precision, the highest available vector width on the machine
    PRECISION=single make

    # Compile with single precision and vector width 4
    PRECISION=single STATIC_VECTOR_WIDTH=4 make

    # Compile with icc
    CXX=icc make

One can also compile with a different vector backend,

    # Compile with VCL backend
    mkdir vectorclass && cd vectorclass
    wget http://www.agner.org/optimize/vectorclass.zip
    unzip vectorclass.zip
    cd ..
    VECTOR_BACKEND=vcl make

    # Compile just scalar code
    VECTOR_BACKEND=none make
