#include "Compare.h"

// #define M_LOG2E 1.44269504088896340736 //log2(e)
inline long double log2(const long long x) {
    return log(x) * M_LOG2E;
}

bool compare(const double& d1, const double& d2, const double max_epsilon, const bool doPrint) {
  const long long* i1 = reinterpret_cast<const long long*>(&d1);
  const long long* i2 = reinterpret_cast<const long long*>(&d2);

  long long epsilon = 0;
  epsilon = std::max(epsilon, std::abs(*i1 - *i2));
  
  if (epsilon > max_epsilon) {
    if (doPrint) std::cout << "max epsilon: bit " << floor(log2(epsilon))
      << " (" << d1 << " and " << d2 << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare(const TrackVector& m1, const TrackVector& m2, const double max_epsilon, const bool doPrint) {
  const long long* i1;
  const long long* i2;
  double d1, d2;

  long long epsilon = 0;
  for (unsigned i=0; i<5; ++i) {
    i1 = reinterpret_cast<const long long*>(&m1.fArray[i]);
    i2 = reinterpret_cast<const long long*>(&m2.fArray[i]);

    long long prevEpsilon = epsilon;
    epsilon = std::max(epsilon, std::abs(*i1 - *i2));

    if (prevEpsilon != epsilon) {
      d1 = m1.fArray[i];
      d2 = m2.fArray[i];
    }
  }
  
  if (epsilon > max_epsilon) {
    if (doPrint) std::cout << "max epsilon: bit " << floor(log2(epsilon))
      << " (" << d1 << " and " << d2 << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare(const TrackMatrix& m1, const TrackMatrix& m2, const double max_epsilon, const bool doPrint) {
  const long long* i1;
  const long long* i2;
  double d1, d2;

  long long epsilon = 0;
  for (unsigned i=0; i<25; ++i) {
    i1 = reinterpret_cast<const long long*>(&m1.fArray[i]);
    i2 = reinterpret_cast<const long long*>(&m2.fArray[i]);

    long long prevEpsilon = epsilon;
    epsilon = std::max(epsilon, std::abs(*i1 - *i2));
    
    if (prevEpsilon != epsilon) {
      d1 = m1.fArray[i];
      d2 = m2.fArray[i];
    }
  }
  
  if (epsilon > max_epsilon) {
    if (doPrint) std::cout << "max epsilon: " << "bit " << floor(log2(epsilon))
      << " (" << d1 << " and " << d2 << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare(const TrackSymMatrix& m1, const TrackSymMatrix& m2, const double max_epsilon, const bool doPrint) {
  const long long* i1;
  const long long* i2;
  double d1, d2;

  long long epsilon = 0;
  for (unsigned i=0; i<15; ++i) {
    i1 = reinterpret_cast<const long long*>(&m1.fArray[i]);
    i2 = reinterpret_cast<const long long*>(&m2.fArray[i]);

    long long prevEpsilon = epsilon;
    epsilon = std::max(epsilon, std::abs(*i1 - *i2));

    if (prevEpsilon != epsilon) {
      d1 = m1.fArray[i];
      d2 = m2.fArray[i];
    }
  }
  
  if (epsilon > max_epsilon) {
    if (doPrint) std::cout << "max epsilon: " << "bit " << floor(log2(epsilon))
      << " (" << d1 << " and " << d2 << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare(const ChiSquare& c1, const ChiSquare& c2, const double max_epsilon, const bool doPrint) {
  const long long* i1 = reinterpret_cast<const long long*>(&c1.m_chi2);
  const long long* i2 = reinterpret_cast<const long long*>(&c2.m_chi2);

  long long epsilon = 0;
  bool equal = true;

  epsilon = std::max(epsilon, std::abs(*i1 - *i2));
  if (epsilon > max_epsilon) {
    equal = false;
    if (doPrint) std::cout << "Chi2 mismatch " << "(" << "bit " << floor(log2(epsilon)) << ") "
      << "(" << c1.m_chi2 << ", " << c2.m_chi2 << ")" << std::endl;
  }

  if (c1.m_nDoF != c2.m_nDoF) {
    equal = false;
    if (doPrint) std::cout << "nDoF mismatch " << "(" << c1.m_nDoF << ", " << c2.m_nDoF << ")" << std::endl;
  }

  return equal;
}

bool compare(const State& s1, const State& s2, const double max_epsilon, const bool doPrint) {
  const long long* i1 = reinterpret_cast<const long long*>(&s1.m_z);
  const long long* i2 = reinterpret_cast<const long long*>(&s2.m_z);

  bool equal = true;

  if (!compare(s1.m_stateVector, s2.m_stateVector, max_epsilon, doPrint)) {
    if (doPrint) std::cout << "StateVector mismatch" << std::endl;
    equal = false;
  }

  if (!compare(s1.m_covariance, s2.m_covariance, max_epsilon, doPrint)) {
    if (doPrint) std::cout << "Covariance mismatch" << std::endl;
    equal = false;
  }

  return equal;
}

bool compare(
  const FitNode& generatedFitNode,
  const FitNode& expectedFitNode,
  const double max_epsilon,
  const bool doPrint,
  const int compareChi2
) {
  // For now, let's compare the matrices
  bool equal = true;

  auto test = [&equal, &doPrint] (const bool& result, const std::string& printout) {
    if (!result) {
      equal = false;
      if (doPrint) {
        std::cout << printout << std::endl;
      }
    }
  };
  
  // Compare states
  if (expectedFitNode.m_hasInfoUpstream[0] or expectedFitNode.m_type == HitOnTrack) {
    test(compare(generatedFitNode.m_filteredState[0], expectedFitNode.m_filteredState[0], max_epsilon, doPrint), "Forward updated state mismatch");
  }
  if (expectedFitNode.m_hasInfoUpstream[1] or expectedFitNode.m_type == HitOnTrack) {
    test(compare(generatedFitNode.m_filteredState[1], expectedFitNode.m_filteredState[1], max_epsilon, doPrint), "Backward updated state mismatch");
  }
  
  test(compare(generatedFitNode.m_state, expectedFitNode.m_state, max_epsilon, doPrint), "Smoothed State mismatch");

  if (compareChi2 == 0)
    test(compare(generatedFitNode.m_totalChi2[0], expectedFitNode.m_totalChi2[0], max_epsilon, doPrint), "Forward chi2 mismatch");
  else if (compareChi2 == 1)
    test(compare(generatedFitNode.m_totalChi2[1], expectedFitNode.m_totalChi2[1], max_epsilon, doPrint), "Backward chi2 mismatch");

  test(compare(generatedFitNode.m_residual, expectedFitNode.m_residual, max_epsilon, doPrint), "Residual mismatch");
  test(compare(generatedFitNode.m_errResidual, expectedFitNode.m_errResidual, max_epsilon, doPrint), "Error Residual mismatch");

  return equal;
}
